import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    static int M;
    static double[] x;
    static double[] y;
    static int NUMBER_OF_PARAMETER = 7;
    static double[] theta = new double[NUMBER_OF_PARAMETER];
    static double alpha = 1;
    static double THRESHOLD = 1E-6;
    static int[] enabled = {2, 3};

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        M = in.nextInt();
        x = new double[M + 1];
        y = new double[M + 1];
        x[0] = 1;
        y[0] = 0;
        double xm = 0;
        double ym = 0;
        for (int i = 1; i <= M; i++) {
            x[i] = in.nextInt();
            y[i] = in.nextInt();
            xm += x[i];
            ym += y[i];
        }

        xm = xm / M;
        ym = ym / M;
        normalize(xm, ym);

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        //initTheta();
        //theta[6] = 0;
        compute(10000);
        System.out.println(Arrays.toString(theta));
        System.err.println(J());
        System.out.println("O(1)");
    }

    static void normalize(double xm, double ym) {
        for (int i = 1; i <= M; i++) {
            x[i] = (x[i] - xm) / xm;
            y[i] = (y[i] - ym) / ym;
        }
    }

    static void initTheta() {
        double t = 0.1;
        for (int i = 0; i < NUMBER_OF_PARAMETER; i++) {
            theta[i] = (i + 1) * t;
        }
    }

    static boolean contains(int i, int[] array) {
        for (int j = 0; j < array.length; j++) {
            if (i == array[j]) {
                return false;
            }
        }
        return true;
    }

    static double X(double x, int i) {
        if(contains(i, enabled)) {
            return 0;
        }
        switch (i) {
            case 0:
                return 1;
            case 1:
                if (x > 0) {
                    return Math.log(x);
                }
                return 0;
            case 2:
                return x;
            case 3:
                if (x <= 0) {
                    return 0;
                }
                return x * Math.log(x);
            case 4:
                return x * x;
            case 5:
                if (x <= 0) {
                    return 0;
                }
                return x * x * Math.log(x);
            case 6:
                return x * x * x;
            default:
                return 0;
        }
    }

    static double h(double x) {
        double result = 0;
        for (int i = 0; i < NUMBER_OF_PARAMETER; i++) {
            result += theta[i] * X(x, i);
        }
        return result;
    }

    static double J() {
        double result = 0;
        for (int i = 1; i <= M; i++) {
            double diff = h(x[i]) - y[i];
            result += diff * diff;
        }
        return result / (2 * M);
    }

    static double derivativeJ(int i) {
        double sum = 0.0;
        for (int j = 1; j <= M; j++) {
            double xj = X(x[j], i);
            sum += (h(x[j]) - y[j]) * xj;
        }
        return sum / M;
    }

    static void updateTheta() {
        double[] thetaCopy = new double[NUMBER_OF_PARAMETER];
        for (int i = 0; i < NUMBER_OF_PARAMETER; i++) {
            double derive = derivativeJ(i);
            thetaCopy[i] = theta[i] - alpha * derive;
        }
        theta = Arrays.copyOf(thetaCopy, NUMBER_OF_PARAMETER);
    }

    static void compute(int time) {
        for (int i = 0; i < time; i++) {
            updateTheta();
        }
    }

    static void compute() {
        int percent = 10000;
        double cost = J();
        while (cost > THRESHOLD) {
            updateTheta();
            if (cost < percent * THRESHOLD) {
                percent /= 10;
                System.out.println(Arrays.toString(theta));
                System.err.println(cost);
            }
            cost = J();
        }
    }
}